final String NAME_KEY = "name";
final String FIRST_NAME_KEY = "first_name";
final String STREET_KEY = "street";
final String ZIP_KEY = "zip";
final String CITY_KEY = "city";
final String MAIL_KEY = "mail";
final String PHONE_KEY = "phone";
