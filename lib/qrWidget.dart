import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QrWidget extends StatelessWidget {
  final String title;

  QrWidget({
    this.title = "QR Code",
  });

  Widget build(BuildContext context) {
    // final Map data = ModalRoute.of(context).settings.arguments;
    var data = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            QrImage(
              data: jsonEncode(data),
              version: QrVersions.auto,
              size: 200.0,
              foregroundColor: Colors.white,
            ),
            Text(jsonEncode(data)),
          ],
        ),
      ),
    );
  }
}
