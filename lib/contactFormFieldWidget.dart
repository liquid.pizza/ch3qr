import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ContactFormFieldWidget extends StatelessWidget {
  final String content;
  final String Function(String) validator;
  final void Function(String) onChanged;

  final bool enabled;
  final String initalValue;

  ContactFormFieldWidget({
    this.content,
    this.validator,
    this.onChanged,
    this.enabled = true,
    this.initalValue,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextFormField(
          decoration: InputDecoration(
            hintText: 'Enter your ${this.content.toLowerCase()}',
            labelText: "${this.content}",
            border: OutlineInputBorder(),
            errorStyle: TextStyle(
              color: Theme.of(context).errorColor, // or any other color
            ),
          ),
          validator: (this.validator == null)
              ? (value) {
                  return null;
                }
              : this.validator,
          onChanged: (this.onChanged == null)
              ? (value) {
                  return null;
                }
              : this.onChanged,
          enabled: this.enabled,
          initialValue: this.initalValue,
        ));
  }
}
