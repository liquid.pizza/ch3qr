import 'dart:convert';

import 'package:ch3qr/contactFormFieldWidget.dart';
import 'package:ch3qr/keyNames.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ScanResultWidget extends StatelessWidget {
  final String title;
  final _formKey = GlobalKey<FormState>();

  ScanResultWidget({
    this.title = "Scan result",
  });

  Widget build(BuildContext context) {
    Map<String, dynamic> data = ModalRoute.of(context).settings.arguments;

    String notEmptyValidator(String value) {
      if (value.isEmpty) {
        return "No valid input.";
      } else {
        return null;
      }
    }

    String getValueIfExists(String key) {
      return data.containsKey(key) ? data[key].toString() : "";
    }

    print(data);
    print("".isEmpty);

    if (data.isEmpty) {
      return Scaffold(
        appBar: AppBar(
          title: Text(this.title),
        ),
        body: Center(child: Text("No valid data.")),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
      ),
      body: Center(
        child: Form(
          key: this._formKey,
          autovalidate: true,
          child: ListView(
            children: [
              ContactFormFieldWidget(
                content: "Name",
                initalValue: getValueIfExists(NAME_KEY),
                enabled: false,
                validator: (value) => notEmptyValidator(value),
              ),
              ContactFormFieldWidget(
                content: "First name",
                initalValue: getValueIfExists(FIRST_NAME_KEY),
                validator: (value) => notEmptyValidator(value),
                enabled: false,
              ),
              ContactFormFieldWidget(
                content: "Street",
                initalValue: getValueIfExists(STREET_KEY),
                validator: (value) => notEmptyValidator(value),
                enabled: false,
              ),
              ContactFormFieldWidget(
                content: "ZIP",
                initalValue: getValueIfExists(ZIP_KEY),
                validator: (value) => notEmptyValidator(value),
                enabled: false,
              ),
              ContactFormFieldWidget(
                content: "City",
                initalValue: getValueIfExists(CITY_KEY),
                validator: (value) => notEmptyValidator(value),
                enabled: false,
              ),
              ContactFormFieldWidget(
                content: "Mail address",
                initalValue: getValueIfExists(MAIL_KEY),
                validator: (value) => notEmptyValidator(value),
                enabled: false,
              ),
              ContactFormFieldWidget(
                content: "Phone number",
                initalValue: getValueIfExists(PHONE_KEY),
                validator: (value) => notEmptyValidator(value),
                enabled: false,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (this._formKey.currentState.validate()) {
            print("data:\n$data");
          } else {
            print("No valid form");
          }
        },
        tooltip: 'Upload',
        child: Icon(Icons.file_upload),
      ),
    );
  }
}
