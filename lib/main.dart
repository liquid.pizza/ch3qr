import 'dart:convert';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:ch3qr/contactFormFieldWidget.dart';
import 'package:ch3qr/keyNames.dart';
import 'package:ch3qr/qrWidget.dart';
import 'package:ch3qr/scanResultWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      routes: {
        "/": (context) => MyHomePage(title: 'ch3qr - simple QR check in'),
        "/qr_code": (context) => QrWidget(),
        "/scan_result": (context) => ScanResultWidget(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();

  Map<String, String> _formData = {};
  ScanResult scanResult;

  Future scan() async {
    try {
      var options = ScanOptions(
          // strings: {
          //   "cancel": _cancelController.text,
          //   "flash_on": _flashOnController.text,
          //   "flash_off": _flashOffController.text,
          // },
          // restrictFormat: selectedFormats,
          // useCamera: _selectedCamera,
          // autoEnableFlash: _autoEnableFlash,
          // android: AndroidOptions(
          //   aspectTolerance: _aspectTolerance,
          //   useAutoFocus: _useAutoFocus,
          // ),
          );

      var result = await BarcodeScanner.scan(options: options);

      Map<String, dynamic> data = {};

      String rawString = result.rawContent;

      try {
        data = json.decode(rawString);
      } catch (e) {
        print("No valid data:\n$rawString");
      }

      Navigator.pushNamed(
        context,
        "/scan_result",
        arguments: data,
      );
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        print(result);
        scanResult = result;
      });
    }
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return !regex.hasMatch(value);
  }

  void updateData(key, value) {
    this._formData.update(key, (oldValue) => value, ifAbsent: () => value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_a_photo),
            tooltip: "Scan",
            onPressed: scan,
          )
        ],
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              ContactFormFieldWidget(
                content: "Name",
                onChanged: (value) {
                  updateData(NAME_KEY, value);
                },
              ),
              ContactFormFieldWidget(
                content: "First name",
                onChanged: (value) {
                  updateData(FIRST_NAME_KEY, value);
                },
              ),
              ContactFormFieldWidget(
                content: "Street",
                onChanged: (value) {
                  updateData(STREET_KEY, value);
                },
              ),
              ContactFormFieldWidget(
                content: "ZIP",
                onChanged: (value) {
                  updateData(ZIP_KEY, value);
                },
              ),
              ContactFormFieldWidget(
                content: "City",
                onChanged: (value) {
                  updateData(CITY_KEY, value);
                },
              ),
              ContactFormFieldWidget(
                content: "Mail address",
                validator: (value) {
                  return validateEmail(value)
                      ? "Please enter a valid address."
                      : null;
                },
                onChanged: (value) {
                  updateData(MAIL_KEY, value);
                },
              ),
              ContactFormFieldWidget(
                content: "Phone number",
                onChanged: (value) {
                  updateData(PHONE_KEY, value);
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (this._formKey.currentState.validate()) {
            print("data:\n${this._formData}");
            Navigator.pushNamed(
              context,
              "/qr_code",
              arguments: this._formData,
            );
          } else {
            print("No valid form");
          }
        },
        tooltip: 'Create QR code',
        child: Icon(Icons.contacts),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
